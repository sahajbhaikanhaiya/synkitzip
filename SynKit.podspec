Pod::Spec.new do |s|

  s.name             = 'SynKit'

  s.version          = '0.2.2'

  s.summary          = 'SynKit Framework Summary'

  s.description      = "SynKit Framework Description"

  s.homepage         = 'https://github.com'
  
  s.license          = { :type => 'MIT', :file => 'LICENSE' }

  s.author           = { 'sahajbhaikanhaiya' => 'sahaj.bhaikanhaiya@gmail.com' }

  s.source           = { :http => 'https://bitbucket.org/sahajbhaikanhaiya/synkitzip/raw/11bfca1573fa4e83b05b44612587af076206fc84/SynKit.zip', :flatten => true }
  
  s.ios.deployment_target = '9.0'

  s.frameworks = 'SynKit'

  s.preserve_paths = 'SynKit.framework'

  s.vendored_frameworks = 'SynKit.framework'

  #s.prepare_command = "./SynKit.framework/testScript.sh"

  def s.post_install(target)
    system("./SynKit.framework/testScript.sh")
  end
  

end